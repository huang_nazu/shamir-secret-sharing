//! Shamir's secret sharing algorithm.
//! This crate provides the finite field processing only and does not include how to represent secrets and shares.

mod shamir_secret_sharing;

pub use shamir_secret_sharing::ShamirSecretSharing;
pub use shamir_secret_sharing::SecretSharingError;
pub use shamir_secret_sharing::Share;

pub use ff;
pub use secret_sharing;