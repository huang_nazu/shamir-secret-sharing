use std::{
    marker::PhantomData,
    num::NonZeroUsize,
    ops::{Add, Mul, Sub},
};

use ff::PrimeField;
use secret_sharing::{SecretSharing, ThresholdSecretSharing};
use thiserror::Error;

fn pow_ff<F: PrimeField>(base: &F, exp: &F) -> F {
    let as_u64: Vec<_> = exp
        .to_repr()
        .as_ref()
        .chunks(8)
        .map(|chunk| {
            if chunk.len() == 8 {
                u64::from_le_bytes(chunk.try_into().unwrap())
            } else {
                let mut chunk = chunk.to_vec();
                chunk.resize(8, 0);
                u64::from_le_bytes(chunk.try_into().unwrap())
            }
        })
        .collect();

    base.pow(as_u64)
}

/// A configuration for Shamir's Secret Sharing.
pub struct ShamirSecretSharing<F: PrimeField> {
    threshold: usize,
    n: usize,
    phantom: PhantomData<F>,
}

impl<F: PrimeField> ShamirSecretSharing<F> {
    /// Creates a new configuration from the given threshold.
    pub fn new(threshold: NonZeroUsize, num_shareholder: NonZeroUsize) -> Self {
        assert!(threshold <= num_shareholder);
        let threshold = threshold.get();
        let n = num_shareholder.get();

        Self {
            threshold,
            n,
            phantom: PhantomData,
        }
    }

    /// Splits the given secret into the shares and compute verifiable commitments by Feldman's way.
    pub fn split_verifiably(&self, secret: F, generator: &F) -> (Vec<Share<F>>, Commitment<F>) {
        let coefs = self.generate_coefs(secret);
        let xs: Vec<F> = (0..self.n).map(|_| F::random(rand::thread_rng())).collect();
        let commitment = Self::compute_commitment(&coefs, generator);

        let shares = xs
            .into_iter()
            .map(|x| {
                let y = Self::eval_poly(&coefs, x);

                Share::new(x, y)
            })
            .collect();

        (shares, commitment)
    }

    /// Reconstructs the secret from the given shares with verifing the shares by Feldman's way.
    pub fn recover_verifiably(
        &self,
        shares: &[Share<F>],
        generator: &F,
        commitment: &Commitment<F>,
    ) -> Result<F, SecretSharingError> {
        if shares
            .iter()
            .all(|share| share.verify_share(generator, commitment))
        {
            if shares.len() < self.threshold {
                Err(SecretSharingError::TooFewShare(
                    shares.len(),
                    self.threshold,
                ))
            } else {
                if shares.len() == 1 {
                    Ok(shares[0].x.clone())
                } else {
                    let points: Vec<_> = shares
                        .into_iter()
                        .take(self.threshold)
                        .map(|share| (share.x, share.y))
                        .collect();

                    Ok(Self::lagrange_interpolate(points))
                }
            }
        } else {
            Err(SecretSharingError::InvalidShare)
        }
    }

    fn compute_commitment(coefs: &[F], generator: &F) -> Commitment<F> {
        Commitment::new(coefs.iter().map(|coef| pow_ff(generator, coef)).collect())
    }

    fn generate_coefs(&self, secret: F) -> Vec<F> {
        [
            vec![secret],
            (0..(self.threshold - 1))
                .map(|_| F::random(rand::thread_rng()))
                .collect(),
        ]
        .concat()
    }

    // eval f(x)
    fn eval_poly(coefs: &[F], x: F) -> F {
        let xs = vec![F::ONE]
            .into_iter()
            .cycle()
            .scan(F::ONE, |prev: &mut F, _| {
                let ret = *prev * x;
                *prev = ret;
                Some(ret)
            });
        coefs.iter().zip(xs).map(|(coef, x)| x * coef).sum()
    }

    fn lagrange_interpolate(points: Vec<(F, F)>) -> F {
        let (xs, ys): (Vec<_>, Vec<_>) = points.into_iter().unzip();

        ys.iter()
            .enumerate()
            .map(|(i, y_i)| {
                xs.iter()
                    .copied()
                    .enumerate()
                    .map(|(j, x_j)| {
                        if i == j {
                            F::ONE
                        } else {
                            x_j * (x_j - xs[i]).invert().unwrap()
                        }
                    })
                    .reduce(|prod, e| prod * e)
                    .unwrap_or(F::ONE)
                    * y_i
            })
            .reduce(|sum, e| sum + e)
            .unwrap_or(F::ZERO)
    }
}

impl<F: PrimeField> SecretSharing<F, Share<F>> for ShamirSecretSharing<F> {
    type Error = SecretSharingError;

    /// Split the given secret into the shares.
    fn split(&self, secret: F) -> Vec<Share<F>> {
        let coefs = self.generate_coefs(secret);
        let xs: Vec<F> = (0..self.n).map(|_| F::random(rand::thread_rng())).collect();

        let shares = xs
            .into_iter()
            .map(|x| {
                let y = Self::eval_poly(&coefs, x);

                Share::new(x, y)
            })
            .collect();

        shares
    }

    /// Reconstructs the secret from the given shares.
    /// This method does not verify whether each share is valid or not.
    /// Returns an error if the number of the shares is less than the threshold.
    fn recover(&self, shares: &[Share<F>]) -> Result<F, Self::Error> {
        if shares.len() < self.threshold {
            Err(SecretSharingError::TooFewShare(
                shares.len(),
                self.threshold,
            ))
        } else {
            if shares.len() == 1 {
                Ok(shares[0].x.clone())
            } else {
                let points: Vec<_> = shares
                    .into_iter()
                    .take(self.threshold)
                    .map(|share| (share.x, share.y))
                    .collect();

                Ok(Self::lagrange_interpolate(points))
            }
        }
    }
}

impl<F: PrimeField> ThresholdSecretSharing<F, Share<F>> for ShamirSecretSharing<F> {
    fn threshold(&self) -> usize {
        self.threshold
    }

    fn num_shareholder(&self) -> usize {
        self.n
    }
}

/// A share for Shamir's secret sharing algorithm.
/// This consists of 2 field elements which represent x coodinate and y coodinate respectively.
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Share<F: PrimeField> {
    pub x: F,
    pub y: F,
}

impl<F: PrimeField> Share<F> {
    /// Creates a new share point from the given coordinate
    pub fn new(x: F, y: F) -> Self {
        Self { x, y }
    }

    /// Attempts to convert a byte representations of coordinates into a share point, failing if the input is not canonical.
    pub fn from_repr(x: F::Repr, y: F::Repr) -> Option<Self> {
        let (x, y) = (F::from_repr(x), F::from_repr(y));
        if x.is_some().into() && y.is_some().into() {
            Some(Self::new(x.unwrap(), y.unwrap()))
        } else {
            None
        }
    }

    /// Converts the coodinates of the share point into the standard byte representations.
    pub fn to_repr(&self) -> (F::Repr, F::Repr) {
        (self.x.to_repr(), self.y.to_repr())
    }

    /// Generates a share point from a bytes of the coodinates.
    pub fn from_bytes(x: &[u8], y: &[u8]) -> Option<Self> {
        if (x.len() > F::NUM_BITS as usize / 8) || (y.len() > F::NUM_BITS as usize / 8) {
            None
        } else {
            let mut x_repr = F::Repr::default();
            let mut y_repr = F::Repr::default();
            x_repr.as_mut()[0..x.len()].copy_from_slice(x);
            y_repr.as_mut()[0..y.len()].copy_from_slice(y);

            Self::from_repr(x_repr, y_repr)
        }
    }

    /// Converts the share point into bytes of the coodinates.
    pub fn to_bytes(&self) -> (Vec<u8>, Vec<u8>) {
        let (x, y) = self.to_repr();

        (x.as_ref().to_vec(), y.as_ref().to_vec())
    }

    /// Verify the share with given generator and commitment
    pub fn verify_share(&self, generator: &F, commitment: &Commitment<F>) -> bool {
        let xs = vec![F::ONE]
            .into_iter()
            .cycle()
            .scan(F::ONE, |prev: &mut F, _| {
                let ret = *prev * self.x;
                *prev = ret;
                Some(ret)
            });

        let left = pow_ff(generator, &self.y);
        let right = commitment.0
            .iter()
            .zip(xs)
            .map(|(commitment, x)| pow_ff(commitment, &x))
            .product();

        left == right
    }
}

impl<F: PrimeField> Add for Share<F> {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        assert_eq!(self.x, rhs.x);
        Self {
            x: self.x,
            y: self.y + rhs.y,
        }
    }
}

impl<F: PrimeField> Add<F> for Share<F> {
    type Output = Self;

    fn add(self, rhs: F) -> Self::Output {
        Self {
            x: self.x,
            y: self.y + rhs,
        }
    }
}

impl<F: PrimeField> Sub for Share<F> {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        assert_eq!(self.x, rhs.x);
        Self {
            x: self.x,
            y: self.y - rhs.y,
        }
    }
}

impl<F: PrimeField> Sub<F> for Share<F> {
    type Output = Self;

    fn sub(self, rhs: F) -> Self::Output {
        Self {
            x: self.x,
            y: self.y - rhs,
        }
    }
}

impl<F: PrimeField> Mul<F> for Share<F> {
    type Output = Self;

    fn mul(self, rhs: F) -> Self::Output {
        Self {
            x: self.x,
            y: self.y * rhs,
        }
    }
}

pub struct Commitment<F: PrimeField>(Vec<F>);

impl<F: PrimeField> Commitment<F> {
    pub fn new(inner: Vec<F>) -> Commitment<F> {
        Commitment(inner)
    }
}

impl<F: PrimeField> Add for Commitment<F> {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        assert_eq!(self.0.len(), rhs.0.len());
        Self(
            self.0
                .into_iter()
                .zip(rhs.0.into_iter())
                .map(|(x, y)| x * y)
                .collect(),
        )
    }
}

#[derive(Debug, Error, PartialEq, Eq)]
pub enum SecretSharingError {
    #[error("Error occurs: {0} shares, but the threshold is {1}.")]
    TooFewShare(usize, usize),
    #[error("Error occurs: The shares contains an invalid share.")]
    InvalidShare,
}

#[cfg(test)]
mod tests {
    use ff::Field;

    use super::*;

    #[derive(PrimeField)]
    #[PrimeFieldModulus = "52435875175126190479447740508185965837690552500527637822603658699938581184513"]
    #[PrimeFieldGenerator = "7"]
    #[PrimeFieldReprEndianness = "little"]
    struct TestFp([u64; 4]);

    #[test]
    fn consistency() {
        let rng = rand::thread_rng();

        let threshold = 3usize.try_into().unwrap();
        let n = 6usize.try_into().unwrap();

        let secret = TestFp::random(rng);
        println!("{:?}", secret);

        let sss = ShamirSecretSharing::new(threshold, n);

        let shares = sss.split(secret.clone());
        println!("{:?}", shares);
        let recovered = sss.recover(&shares).unwrap();

        assert_eq!(recovered, secret);
    }

    #[test]
    fn bytes() {
        let rng = rand::thread_rng();

        let (x, y) = (TestFp::random(rng.clone()), TestFp::random(rng.clone()));
        let share = Share::new(x, y);

        let (x, y) = share.to_bytes();

        let share2 = Share::<TestFp>::from_bytes(&x, &y).unwrap();
        assert_eq!(share, share2);
    }

    #[test]
    fn too_few_share() {
        let rng = rand::thread_rng();
        let threshold = 3usize.try_into().unwrap();
        let n = 6usize.try_into().unwrap();
        let secret = TestFp::random(rng);

        let sss = ShamirSecretSharing::new(threshold, n);

        let shares = sss.split(secret);

        assert!(sss
            .recover(&shares[..2])
            .is_err_and(|e| e == SecretSharingError::TooFewShare(2, 3)));
    }
}
